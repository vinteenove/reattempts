<?php

namespace Reattempts;

/**
 * Class Reattempts
 *
 * @package Reattempts
 *
 *  ## EXEMPLE
 *  $reattempt = new Reattempts();
 *  $reattempt->setAttemptsDefault(5);
 *  $reattempt->addInterval('PT5H');
 *  $reattempt->addInterval('PT1H');
 *  $reattempt->addInterval('PT12H');
 *  $reattempt->addInterval('P1D');
 *
 *  $whereReattempt = $reattempt->generateWhereQuery('attempts', 'createdAt');
 *
 */
class Reattempts
{
	/**
	 * @var int
	 */
	private $attemptsDefault = 4;
	private $attemptsCurrent;
	private $attempts;
	private $last;
	
	
	public function __construct(
		$attemptsDefault = 4
	){
		$this->setAttemptsDefault($attemptsDefault);
	}
	
	/**
	 * @param int $attemptsDefault
	 */
	public function setAttemptsDefault(int $attemptsDefault): void
	{
		$this->attemptsDefault = $this->attemptsCurrent = $attemptsDefault;
	}
	
	/**
	 * Adicina um intervalo com referência na hora atual
	 *
	 * @param string $interval
	 * @throws \Exception
	 */
	public function addInterval(string $interval): void
	{
		unset($this->last);
		
		$this->last = [
			'attempt'  => ($this->attemptsCurrent++),
			'interval' => (new \DateTime)->sub(new \DateInterval($interval)),
		];
		
		$this->attempts[] =& $this->last;
	}
	
	/**
	 * Adicina um intervalo com referência ao horario adicionado anteriomente.
	 *
	 * Voce consegue configurar
	 *
	 * @param string $interval
	 * @param int    $repeat
	 * @throws \Exception
	 */
	public function nextInterval(string $interval, int $repeat = 1): void
	{
		for($i = 0; $i < $repeat; $i++){
			$prevDate = ($this->last['interval'] ?? (new \DateTime));
			$prevDate = clone $prevDate;
			
			unset($this->last);
			
			$this->last = [
				'attempt'  => ($this->attemptsCurrent++),
				'interval' => $prevDate->sub(new \DateInterval($interval)),
			];
			
			$this->attempts[] =& $this->last;
		}
	}
	
	/**
	 * Gera uma query em sql para consulta em do intervalo esperado
	 *
	 * @param string $columnAttempts
	 * @param string $columnInsertedAt
	 * @return string
	 * @throws \Exception
	 */
	public function generateWhereQuery(string $columnAttempts = 'attempts', string $columnInsertedAt = 'insertedAt'): string
	{
		$where = ['(`' . $columnAttempts . '` < "' . $this->attemptsDefault . '")'];
		
		foreach($this->attempts as $intervals){
			$where[] = '(`' . $columnAttempts . '` = "' . $intervals['attempt'] . '" AND `' . $columnInsertedAt . '` < "' . $intervals['interval']->format('Y-m-d H:i') . '")';
		}
		
		return implode(' OR ', $where);
	}
	
	public function __toString(): string
	{
		return $this->generateWhereQuery();
	}
}