# Reattempts
Essa classe visa gerar uma where automaticamente para o teste de retentavivas ao decorrer de um periodo estipulado.


## Get start

```php
 $reattempt = new Reattempts();
 $reattempt->setAttemptsDefault(5);
 $reattempt->addInterval('PT5H');
 $reattempt->addInterval('PT12H');
 $reattempt->addInterval('P1D');

$whereReattempt = $reattempt->generateWhereQuery('attempts', 'createdAt');
``` 

O exemplo acima irá gerar um where para pesquisa dentro dos intervalos especificados
 
* `setAttemptsDefault(5)` define que será realizada 5 tentativas padrões
* `addInterval('PT1H')` define o intervalo para próxima tentativa de 5 Horas
* `addInterval('PT12H')` define o intervalo para próxima tentativa de 12 Horas* `addInterval('P1D')` define o intervalo para próxima tentativa de 1 dia

Para define os intervalos é utilizada a notação [dateinterval]

## Resumo da classe

```php
class Reattempts(){

  public function __construct(int $attemptsDefault = 4)
  public function setAttemptsDefault(int $attemptsDefault): void
  public function addInterval(string $interval): void
  public function nextInterval(string $interval, int $repeat = 1): void
  public function generateWhereQuery(string $columnAttempts = 'attempts', string $columnInsertedAt = 'insertedAt'): string
  public function __toString(): string

}
```

## Métodos

> `public function __construct(int $attemptsDefault = 4)`
>
> Constrói o objeto



> `public function setAttemptsDefault(int $attemptsDefault): void`
>
> Configura as tentativas



> `public function addInterval(string $interval): void`
> 
> Adiciona um intervalo com referência ao horario atual



> `public function nextInterval(string $interval, int $repeat = 1): void`
>
> Adiciona um intervalo com referência ao intervalo anterior. Repetir o processo adicina mais intervalos com a mesma configuração.



> `public function generateWhereQuery(string $columnAttempts = 'attempts', string $columnInsertedAt = 'insertedAt'): string`
>
> Retorna a query gerada



> `public function __toString(): string`
>
> Retorna a query gerada com as configurações padrões


[dateinterval]: https://www.php.net/manual/en/class.dateinterval.php